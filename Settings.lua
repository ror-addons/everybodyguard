
local Settings={}
EveryBodyGuard.Settings=Settings
local SettingsWindow = "EveryBodyGuard_Settings_ScrollWindow_ScrollChild_"

local pairs=pairs
local ipairs=ipairs

local DisplayIndex,DisplayLock

function Settings.Create()
	CreateWindow("EveryBodyGuard_Settings",false)
	Settings.SetLabels()
	WindowSetHandleInput("EveryBodyGuard_Settings",false)
	WindowSetShowing("EveryBodyGuard_Settings",false)
	Fader.Register("EveryBodyGuard_Settings",0.75)
end

function Settings.Open()
	Settings.PopulateAll()
	if(not WindowGetHandleInput("EveryBodyGuard_Settings"))
	then
		WindowSetHandleInput("EveryBodyGuard_Settings",true)
		Fader.FadeTo("EveryBodyGuard_Settings",1)
	end
end

function Settings.Close()
	if(WindowGetHandleInput("EveryBodyGuard_Settings"))
	then
		WindowSetHandleInput("EveryBodyGuard_Settings",false)
		Fader.FadeTo("EveryBodyGuard_Settings",0)
	end
end

function Settings.SetLabels()
	local list=		{
						["Display"]							= L"Display",
						["ShowHeadIcon"]					= L"Show Head Icon",
						["HotkeyReassign"]					= L"Automatically Reassign Hotkeys"
					}
	for i,k in pairs(list)
	do
		LabelSetText(SettingsWindow..i.."Text",k)
	end

		list=		{
						--[""]								= L"",
					}
	for i,k in pairs(list)
	do
		ButtonSetText(SettingsWindow..i,k)
	end
end

function Settings.ButtonClicked()
	local button=SystemData.ActiveWindow.name
	if(ButtonGetDisabledFlag(button))then return end
	local _,_,name=string.find(button,"[_,%w]+_(%w+)")
	if(Settings[name])then Settings[name]() end
end

function Settings.ComboBoxChanged()
	local box=SystemData.ActiveWindow.name
	if(ComboBoxGetDisabledFlag(box))then return end
	local idx=ComboBoxGetSelectedMenuItem(box)
	local _,_,name=string.find(box,"[_,%w]+_(%w+)ComboBox")
	name=name.."Changed"
	if(Settings[name])then Settings[name](idx) end
end

function Settings.ToggleCheckBox()
	local check=SystemData.ActiveWindow.name
	local button=check.."Button"
	if(ButtonGetDisabledFlag(button))then return end
	local checked=not ButtonGetPressedFlag(button)
	ButtonSetPressedFlag(button,checked)
	local _,_,name=string.find(check,"[_,%w]+_(%w+)CheckBox")
	name=name.."Changed"
	if(Settings[name])then Settings[name](checked) end
end

function Settings.PopulateAll()
	Settings.PopulateDisplay()
	Settings.PopulateShowHeadIcon()
	Settings.PopulateHotkeyReassign()
end

function Settings.PopulateDisplay()
	DisplayLock=true
	local box=SettingsWindow.."Display".."ComboBox"
	ComboBoxClearMenuItems(box)
	DisplayIndex={}
	local t=1
	for i,k in pairs(EveryBodyGuard.DisplayModes)
	do
		if(k.IsAviable())
		then
			ComboBoxAddMenuItem(box,towstring(i))
			DisplayIndex[t]=i
			DisplayIndex[i]=t
			t=t+1
		end
	end
	ComboBoxSetSelectedMenuItem(box,DisplayIndex[EveryBodyGuard.Data.Display])
	DisplayLock=false
end

function Settings.PopulateShowHeadIcon()
	local button=SettingsWindow.."ShowHeadIcon".."CheckBoxButton"
	ButtonSetPressedFlag(button,EveryBodyGuard.Data.ShowHeadIcon)
end

function Settings.PopulateHotkeyReassign()
	local button=SettingsWindow.."HotkeyReassign".."CheckBoxButton"
	ButtonSetPressedFlag(button,EveryBodyGuard.Data.HotkeyReassign)
end

function Settings.DisplayChanged(idx)
	EveryBodyGuard.ActivateDisplay(DisplayIndex[idx])
	if(not DisplayLock)then Settings.PopulateDisplay() end
end

function Settings.ShowHeadIconChanged(check)
	EveryBodyGuard.Data.ShowHeadIcon=check
	EveryBodyGuard.GroupUpdated()
end

function Settings.HotkeyReassignChanged(check)
	EveryBodyGuard.Data.HotkeyReassign=check
	EveryBodyGuard.ActivateHotkeys(check)
end
