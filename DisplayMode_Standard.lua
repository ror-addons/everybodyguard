local Standard={}
EveryBodyGuard.DisplayModes.Standard=Standard

local window_="EveryBodyGuard_"
local windows,UpdateCache
local width,height
local healthcolor=	{
						{200,  0,0},
						{200,100,0},
						{200,200,0},
						{100,200,0},
						{  0,200,0}
					}

local ipairs=ipairs
local unpack=unpack
local GetIconData=GetIconData
local LabelSetText=LabelSetText
local WindowSetDimensions=WindowSetDimensions
local WindowSetTintColor=WindowSetTintColor
local DynamicImageSetTexture=DynamicImageSetTexture
local mf=math.floor
local ti=table.insert
local ts=table.sort

function Standard.Initialize()
	if(not Standard.IsAviable())then return end
	windows={}
	for i=1,5
	do
		windows[i]=window_..i
	end
	for i,window in ipairs(windows)
	do
		if(DoesWindowExist(window))
		then
			local parentpoint,point,parent,x,y=WindowGetAnchor(window,1)
			local scale=WindowGetScale(window)
			DestroyWindow(window)
			CreateWindowFromTemplate(window,"EBG_MemberContainer","Root")
			WindowClearAnchors(window)
			WindowAddAnchor(window,point,parent,parentpoint,x,y)
			WindowSetScale(window,scale)
		else
			CreateWindowFromTemplate(window,"EBG_MemberContainer","Root")
			LayoutEditor.RegisterWindow(window,towstring(i)..L"-EveryBodyGuard",L"switch guard",false,false,true,nil)
		end
		WindowSetShowing(window,false)
		Fader.Register(window.."Target",0.19,nil,nil,true)
		Fader.Register(window.."GuardBlocked",nil,nil,nil,true)
		Fader.Register(window.."GuardS",0.35,nil,nil,true)
		Fader.Register(window.."GuardH",0.35,nil,nil,true)
	end
	width,height=WindowGetDimensions(window_.."1Bar")
end

function Standard.Shutdown()
	for i,window in ipairs(windows)
	do
		WindowSetShowing(window,false)
		Fader.Unregister(window.."Target")
		Fader.Unregister(window.."GuardS")
		Fader.Unregister(window.."GuardH")
	end
	windows=nil
end

function Standard.IsAviable()
	return true
end

function Standard.GetActionWindow(idx)
	return windows[idx]
end

function Standard.UpdateBinding(idx,s1,s2)
	LabelSetText(windows[idx].."Binding",s2 and s1..L"\n"..s2 or s1)
end

local function UpdateDisplay()
	local window,data
	for idx,member in ipairs(UpdateCache)
	do
		window=windows[idx]
		data=member[1]
		if(data)
		then
			WindowSetShowing(window,true)
			LabelSetText(window.."Mem",data.name)
			local tex,x,y=GetIconData(Icons.GetCareerIconIDFromCareerLine(data.careerLine))
			DynamicImageSetTexture(window.."Icon",tex,x,y)
			Fader.FadeTo_MultiCallSafe(window.."GuardBlocked",0,0.2)
			Fader.FadeTo_MultiCallSafe(window.."GuardS",member.SoftGuard or 0)
			Fader.FadeTo_MultiCallSafe(window.."GuardH",member.HardGuard or 0)
			Fader.FadeTo_MultiCallSafe(window.."Target",member.Target or 0)
			Standard.UpdateHealth(idx,data.healthPercent)
		else
			WindowSetShowing(window,false)
		end
	end
	UpdateCache=nil
end

function Standard.UpdateMember(idx,data)
	if(idx==1)
	then
		UpdateCache={{data},nil,nil,nil,nil}
	else
		UpdateCache[idx]={data}
		if(idx==5)
		then
			UpdateDisplay()
		end
	end
end

function Standard.UpdateHealth(idx,hp)
	local bar=windows[idx].."Bar"
	if(hp==100)
	then
		WindowSetDimensions(bar,width,height)
		WindowSetTintColor(bar,unpack(healthcolor[5]))
	else
		local tick=mf((hp-1)/25+1)
		WindowSetDimensions(bar,width*tick/5,height)
		if(tick>0)then WindowSetTintColor(bar,unpack(healthcolor[tick])) end
	end
end

function Standard.UpdateTarget(idx,target)
	local alpha=target and 1 or 0
	if(UpdateCache)
	then
		UpdateCache[idx].Target=alpha
	else
		Fader.FadeTo_MultiCallSafe(windows[idx].."Target",alpha)
	end
end

function Standard.UpdateSoftGuard(idx,guard)
	local alpha=guard and 1 or 0
	if(UpdateCache)
	then
		UpdateCache[idx].SoftGuard=alpha
	else
		Fader.FadeTo_MultiCallSafe(windows[idx].."GuardS",alpha)
	end
end

function Standard.UpdateHardGuard(idx,guard)
	local alpha=guard and 1 or 0
	if(UpdateCache)
	then
		UpdateCache[idx].HardGuard=alpha
	else
		local window=windows[idx]
		if(guard)
		then
			Fader.FadeTo_MultiCallSafe(window.."GuardS",0,nil,0.5)
		end
		Fader.FadeTo_MultiCallSafe(window.."GuardH",alpha)
	end
end

function Standard.GuardBlocked(idx)
	local windowname=windows[idx].."GuardBlocked"
	Fader.FadeTo(windowname,1,0.06)
	Fader.FadeTo_MultiCallSafe(windowname,0,0.19,0.1,1)
end

local function getid(window)
	local _,_,idx=string.find(window,window_.."(%d)")
	return tonumber(idx)
end

function Standard.OnLButtonDown()
	EveryBodyGuard.PreClick(getid(SystemData.ActiveWindow.name))
end

function Standard.OnLButtonUp()
	EveryBodyGuard.Click(getid(SystemData.ActiveWindow.name))
end

function Standard.OnRButtonUp()
	EveryBodyGuard.Assist(getid(SystemData.ActiveWindow.name))
end
