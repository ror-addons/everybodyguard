local _Squared={}
EveryBodyGuard.DisplayModes.Squared=_Squared

local EBG_cache
local Squared_cache
local colors=	{["soft"]={180,152,0},["hard"]={255,50,50},["blocked"]={230,230,230}}
local ind_place

local Squared=Squared
local function donothing() end		-- Squared already handles that or we don't care

_Squared.UpdateBinding=donothing
_Squared.UpdateHealth=donothing
_Squared.UpdateTarget=donothing

local function setindicator(unit,color,showing)
	local ind=unit.indicators[ind_place]
	ind.color=color
	ind.showing=showing
end

function _Squared.Initialize()
	if(not _Squared.IsAviable())then return end
	Squared_cache={}
	EBG_cache={}
	Squared.RegisterEventHandler("setname",_Squared.SquaredSetName)
	Squared.RegisterEventHandler("cleargroups",_Squared.SquaredClearGroups)
	Squared.RegisterEventHandler("unitlclick",_Squared.OnLButtonDown)
	Squared.RegisterEventHandler("unitlrelease",_Squared.OnLButtonUp)
	_Squared.SquaredClearGroups()
	Squared.ResetMode()
end

function _Squared.Shutdown()
	for i,k in pairs(Squared_cache)
	do
		setindicator(k,colors.soft,false)
		k:UpdateIndicators()
	end
	Squared_cache=nil
	EBG_cache=nil
	Squared.UnregisterEventHandler("setname",_Squared.SquaredSetName)
	Squared.UnregisterEventHandler("cleargroups",_Squared.SquaredClearGroups)
	Squared.UnregisterEventHandler("unitlclick",_Squared.OnLButtonDown)
	Squared.UnregisterEventHandler("unitlrelease",_Squared.OnLButtonUp)
end

function _Squared.IsAviable()
	return Squared~=nil
end

function _Squared.GetActionWindow(idx)
	local unit=type(idx)=="table" and idx or Squared_cache[EBG_cache[idx]]
	return unit and "SquaredUnit_"..unit.group.."_"..unit.member.."Action"
end

function _Squared.UpdateMember(idx,data)
	local name=EBG_cache[idx]
	if(name)then EBG_cache[name]=nil end
	if(data)
	then
		name=data.name
		EBG_cache[idx]=name
		EBG_cache[name]={["idx"]=idx,["color"]=colors.soft,["showing"]=false}
		if(Squared_cache[name])
		then
			EveryBodyGuard.ReApplyHotkeys(_Squared.GetActionWindow(Squared_cache[name]),idx)
		end
	else
		EBG_cache[idx]=nil
	end
end

function _Squared.UpdateSoftGuard(idx,guard)
	_Squared.UpdateGuard(idx,guard,colors.soft)
end

function _Squared.UpdateHardGuard(idx,guard)
	_Squared.UpdateGuard(idx,guard,colors.hard)
end

function _Squared.GuardBlocked(idx)
	_Squared.UpdateGuard(idx,true,colors.blocked)
end

function _Squared.UpdateGuard(idx,showing,color)
	local name=EBG_cache[idx]
	local unit=Squared_cache[name]
	setindicator(unit,color,showing)
	unit:UpdateIndicators()
	local data=EBG_cache[name]
	data.showing=showing
	data.color=color
end

function _Squared.SquaredSetName(unit)
	Squared_cache[unit.name]=unit
	local data=EBG_cache[unit.name]
	if(data)
	then
		setindicator(unit,data.color,data.showing)
		EveryBodyGuard.ReApplyHotkeys(_Squared.GetActionWindow(unit),data.idx)
	else
		setindicator(unit,colors.soft,false)
	end
end

function _Squared.SquaredClearGroups()
	Squared_cache={}
	local places={["topleft"]=true,["topright"]=true,["bottomleft"]=true,["bottomright"]=true}
	places[Squared.GetSetting("status-debuff")]=nil
	places[Squared.GetSetting("status-hot")]=nil
	places[Squared.GetSetting("status-buff")]=nil
	ind_place=next(places) or "topright"
end

function _Squared.OnLButtonDown(group,member)
	local data=EBG_cache[Squared.GetUnit(group,member).name]
	if(data)then EveryBodyGuard.PreClick(data.idx) end
end

function _Squared.OnLButtonUp(group,member)
	local data=EBG_cache[Squared.GetUnit(group,member).name]
	if(data)then EveryBodyGuard.Click(data.idx) end
end
