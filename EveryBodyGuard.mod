<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="EveryBodyGuard" version="1.2" date="07/31/2010" >

		<VersionSettings gameVersion="1.3.6" windowsVersion="1.0" savedVariablesVersion="1.0" />
		<Author name="Crestor" email="crestor@web.de" />
		<Description text="switch Guard in your Group more effectively" />

		<Dependencies>
			 <Dependency name="LibSlash" optional="true" forceEnable="false" />
			 <Dependency name="Squared" optional="true" forceEnable="false" />
			 <Dependency name="Enemy" optional="true" forceEnable="false" />
		</Dependencies>
		<Files>
			<File name="textures/Textures.xml" />
			<File name="EveryBodyGuard.xml" />
			<File name="TargetInfoFix.lua" />
			<File name="Fader.lua" />
			<File name="EveryBodyGuard.lua" />
			<File name="DisplayMode_Standard.lua" />
			<File name="DisplayMode_Squared.lua" />
			<File name="DisplayMode_Enemy.lua" />
			<File name="DisplayMode_Enemy.xml" />
			<File name="Settings.xml" />
			<File name="Settings.lua" />
		</Files>

		<SavedVariables>
			<SavedVariable name="EveryBodyGuard.Data" />
		</SavedVariables>
		<OnInitialize>
			<CallFunction name="EveryBodyGuard.Initialize" />
		</OnInitialize>
		<OnShutdown>
			<CallFunction name="EveryBodyGuard.Shutdown" />
		</OnShutdown>
		<OnUpdate>
			<CallFunction name="EveryBodyGuard.Update" />
		</OnUpdate>
		<WARInfo>
			<Categories>
				<Category name="BUFFS_DEBUFFS" />
				<Category name="GROUPING" />
				<Category name="CAREER_SPECIFIC" />
				<Category name="COMBAT" />
			</Categories>
			<Careers>
				<Career name="BLACKGUARD" />
				<Career name="IRON_BREAKER" />
				<Career name="BLACK_ORC" />
				<Career name="KNIGHT" />
				<Career name="CHOSEN" />
				<Career name="SWORDMASTER" />
			</Careers>
		</WARInfo>
	</UiMod>
</ModuleFile>
