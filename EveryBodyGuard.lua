
EveryBodyGuard={}
EveryBodyGuard.DisplayModes={}

local ids=			{
						[20]	=1363,
						[24]	=1674,
						[61]	=8013,
						[64]	=8325,
						[100]	=9008,
						[104]	=9325
					}

local id
local headwindow="EBG_Head"
local member,member_nm={},{}
local lasttarget,groupupdated
local self_guard,guard,trust
local action_set,action_blocked
local lowerlimit=2.8
local upperlimit=4
local softlag=lowerlimit*1.1
local hardlag=1
local blocker=0
local timer=0
local headid,headidx=0,0
local actionbarstart=66

local EveryBodyGuard=EveryBodyGuard
local _Fader
local ipairs=ipairs
local pairs=pairs
local TargetInfo=TargetInfo
local GetBuffs=GetBuffs
local BuffSelf=GameData.BuffTargetType.SELF
local BuffFriendlyTarget=GameData.BuffTargetType.TARGET_FRIENDLY
local BuffGroupMemberStart=GameData.BuffTargetType.GROUP_MEMBER_START-1


function EveryBodyGuard.Initialize()
	id=ids[GameData.Player.career.id]
	if(not id)then return end
	_Fader=Fader
	if(LibSlash)
	then
		local open=function() EveryBodyGuard.Settings.Open() end
		LibSlash.RegisterSlashCmd("ebg",open)
		LibSlash.RegisterSlashCmd("EveryBodyGuard",open)
	end
	EveryBodyGuard.LoadDefaults()

	RegisterEventHandler(SystemData.Events.PLAYER_TARGET_UPDATED,			"EveryBodyGuard.TargetChangedEvent")
	RegisterEventHandler(SystemData.Events.PLAYER_EFFECTS_UPDATED,			"EveryBodyGuard.SelfBuffsChanged")
	RegisterEventHandler(SystemData.Events.PLAYER_TARGET_EFFECTS_UPDATED,	"EveryBodyGuard.FriendlyBuffsChanged")
	RegisterEventHandler(SystemData.Events.GROUP_EFFECTS_UPDATED,			"EveryBodyGuard.GroupBuffsChanged")
	RegisterEventHandler(SystemData.Events.GROUP_UPDATED,					"EveryBodyGuard.GroupUpdated")
	RegisterEventHandler(SystemData.Events.SCENARIO_BEGIN,					"EveryBodyGuard.GroupUpdated")
	RegisterEventHandler(SystemData.Events.SCENARIO_END,					"EveryBodyGuard.GroupUpdated")
	RegisterEventHandler(SystemData.Events.BATTLEGROUP_UPDATED,				"EveryBodyGuard.GroupUpdated")
	RegisterEventHandler(SystemData.Events.GROUP_STATUS_UPDATED,			"EveryBodyGuard.GroupStatusUpdated")
	RegisterEventHandler(SystemData.Events.KEYBINDINGS_UPDATED,				"EveryBodyGuard.BindingUpdated")
	RegisterEventHandler(SystemData.Events.PLAYER_BEGIN_CAST,				"EveryBodyGuard.BeginCast")
	RegisterEventHandler(SystemData.Events.PLAYER_ABILITY_TOGGLED,			"EveryBodyGuard.AbilityToggled")

	CreateWindow(headwindow,true)
	_Fader.Register(headwindow,0.35,nil,nil,true)
	LayoutEditor.RegisterWindow(headwindow,L"Head-EveryBodyGuard",L"guard target",false,false,true,nil)
	WindowSetAlpha(headwindow,0)
	WindowRegisterCoreEventHandler(headwindow,"OnShown","EveryBodyGuard.FixOverHeadIcon")
	EveryBodyGuard.ActivateDisplay()

	EveryBodyGuard.Settings.Create()
	if(EveryBodyGuard.Data.HotkeyReassign)then EveryBodyGuard.ActivateHotkeys(true) end
end

function EveryBodyGuard.Shutdown()
	if(LibSlash)
	then
		LibSlash.UnregisterSlashCmd("ebg")
		LibSlash.UnregisterSlashCmd("EveryBodyGuard")
	end
	if(EveryBodyGuard.Data.HotkeyReassign)then EveryBodyGuard.ActivateHotkeys(false) end
	EveryBodyGuard.ActiveDisplay.Shutdown()

	UnregisterEventHandler(SystemData.Events.PLAYER_TARGET_UPDATED,			"EveryBodyGuard.TargetChangedEvent")
	UnregisterEventHandler(SystemData.Events.PLAYER_EFFECTS_UPDATED,		"EveryBodyGuard.SelfBuffsChanged")
	UnregisterEventHandler(SystemData.Events.PLAYER_TARGET_EFFECTS_UPDATED,	"EveryBodyGuard.FriendlyBuffsChanged")
	UnregisterEventHandler(SystemData.Events.GROUP_EFFECTS_UPDATED,			"EveryBodyGuard.GroupBuffsChanged")
	UnregisterEventHandler(SystemData.Events.GROUP_UPDATED,					"EveryBodyGuard.GroupUpdated")
	UnregisterEventHandler(SystemData.Events.SCENARIO_BEGIN,				"EveryBodyGuard.GroupUpdated")
	UnregisterEventHandler(SystemData.Events.SCENARIO_END,					"EveryBodyGuard.GroupUpdated")
	UnregisterEventHandler(SystemData.Events.BATTLEGROUP_UPDATED,			"EveryBodyGuard.GroupUpdated")
	UnregisterEventHandler(SystemData.Events.GROUP_STATUS_UPDATED,			"EveryBodyGuard.GroupStatusUpdated")
	UnregisterEventHandler(SystemData.Events.KEYBINDINGS_UPDATED,			"EveryBodyGuard.BindingUpdated")
	UnregisterEventHandler(SystemData.Events.PLAYER_BEGIN_CAST,				"EveryBodyGuard.BeginCast")
	UnregisterEventHandler(SystemData.Events.PLAYER_ABILITY_TOGGLED,		"EveryBodyGuard.AbilityToggled")
end

function EveryBodyGuard.LoadDefaults()
	if(not EveryBodyGuard.Data)
	then
		EveryBodyGuard.Data=
		{
			version=1.2,
			Display="Standard",
			ShowHeadIcon=true,
			HotkeyReassign=true
		}
	end
end

function EveryBodyGuard.ActivateDisplay(display)
	if(EveryBodyGuard.ActiveDisplay)
	then
		EveryBodyGuard.ActiveDisplay.Shutdown()
	end
	display=display or EveryBodyGuard.Data.Display
	EveryBodyGuard.ActiveDisplay=EveryBodyGuard.DisplayModes[display]
	if(EveryBodyGuard.ActiveDisplay and EveryBodyGuard.ActiveDisplay.IsAviable())
	then
		EveryBodyGuard.Data.Display=display
	else
		EveryBodyGuard.ActiveDisplay=EveryBodyGuard.DisplayModes.Standard
		EveryBodyGuard.Data.Display="Standard"
	end
	EveryBodyGuard.ActiveDisplay.Initialize()
	groupupdated=true
end

function EveryBodyGuard.ActivateHotkeys(activate)
	local bar_name,myt_name,bar_info,myt_info
	for i=1,5
	do
		bar_name="ACTION_BAR_"..(actionbarstart+i)
		myt_name="TARGET_GROUP_MEMBER_"..i
		if(activate)
		then
			bar_info=GetBindingsForAction(bar_name)
			myt_info=GetBindingsForAction(myt_name)
			RemoveAllBindings(myt_name)
			RemoveAllBindings(bar_name)
			for i,k in ipairs(bar_info)
			do
				AddBinding(myt_name,k.deviceId,k.buttons)
			end
			for i,k in ipairs(myt_info)
			do
				AddBinding(bar_name,k.deviceId,k.buttons)
			end
			local window=EveryBodyGuard.ActiveDisplay.GetActionWindow(i)
			if(window)then WindowSetGameActionTrigger(window,GetActionIdFromName(bar_name)) end
		else
			bar_info=GetBindingsForAction(bar_name)
			myt_info=GetBindingsForAction(myt_name)
			RemoveAllBindings(myt_name)
			RemoveAllBindings(bar_name)
			for i,k in ipairs(bar_info)
			do
				AddBinding(myt_name,k.deviceId,k.buttons)
			end
			for i,k in ipairs(myt_info)
			do
				AddBinding(bar_name,k.deviceId,k.buttons)
			end
			local window=EveryBodyGuard.ActiveDisplay.GetActionWindow(i)
			if(window)then WindowSetGameActionTrigger(window,GetActionIdFromName(myt_name)) end
		end
	end
	EveryBodyGuard.BindingUpdated()
end

function EveryBodyGuard.ReApplyHotkeys(window,idx,redirectactive)
	local target=redirectactive==false and "TARGET_GROUP_MEMBER_"..idx or "ACTION_BAR_"..(actionbarstart+idx)
	WindowSetGameActionTrigger(window,GetActionIdFromName(target))
end

local shortkeynames
do
	local LCTRL = 29
	local RCTRL = 157
	local LSHIFT = 42
	local RSHIFT = 54
	local LALT = 56
	local RALT = 184
	shortkeynames=	{
						[LCTRL]=L"Ct",
						[RCTRL]=L"Ct",
						[LSHIFT]=L"Sh",
						[RSHIFT]=L"Sh",
						[LALT]=L"At",
						[RALT]=L"At",
					}
end
local function getbindname(id,device)
	return device==1 and shortkeynames[id] or KeyUtils.ShortenBindingName(GetButtonName(device,id))
end

function EveryBodyGuard.BindingUpdated()
	for i=1,5
	do
		local info=GetBindingsForAction("ACTION_BAR_"..(actionbarstart+i))
		if(#info>0)
		then
			local data=info[1]
			local num=#data.buttons
			if(num>0)
			then
				local s1,s2
				if(num>1)
				then
					s1=getbindname(data.buttons[num-1],data.deviceId)
					s2=getbindname(data.buttons[num],data.deviceId)
				else
					s1=getbindname(data.buttons[num],data.deviceId)
				end
				EveryBodyGuard.ActiveDisplay.UpdateBinding(i,s1,s2)
			else
				EveryBodyGuard.ActiveDisplay.UpdateBinding(i,L"")
			end
		else
			EveryBodyGuard.ActiveDisplay.UpdateBinding(i,L"")
		end
	end
end

function EveryBodyGuard.Update(elapsed)
	if(not id)then return end
	timer=timer+elapsed
	if(timer<0.2)then return end
	if(groupupdated)
	then
		EveryBodyGuard.UpdateGroup()
	else
		for i,k in pairs(member)
		do
			if(k.timer[1]>0)
			then
				k.timer[1]=k.timer[1]-timer
				if(k.timer[1]<=0)
				then
					EveryBodyGuard.DeactivateGuard_Soft(k)
					k.timer[1]=0
				end
			end
		end
	end
	if(not self_guard and guard)
	then
		guard.timer[2]=guard.timer[2]-timer
		if(guard.timer[2]<=0)
		then
			guard.timer[2]=0
			if(lasttarget==guard.name)
			then
				EveryBodyGuard.BuffsChanged(tmp,GetBuffs(BuffFriendlyTarget),true)
			else
				EveryBodyGuard.BuffsChanged(guard,GetBuffs(BuffGroupMemberStart+guard.idx),true,true)
			end
		end
	end
	if(blocker>0)
	then
		blocker=blocker-timer
		if(blocker<0)then blocker=0 end
	end
	timer=0
end

function EveryBodyGuard.GroupUpdated()
	groupupdated=true
end

function EveryBodyGuard.UpdateGroup()
	local data=PartyUtils.GetPartyData()
	local tmp
	local target=TargetInfo:UnitName("selffriendlytarget"):match(L"([^^]+)^?([^^]*)")
	local last_guard=guard
	lasttarget=L""
	guard=nil
	DetachWindowFromWorldObject(headwindow,headid)
	headidx=0
	headid=0
	member={}
	member_nm={}
	for i,k in ipairs(data)
	do
		if(k.name and k.name~=L"")
		then
			tmp={["idx"]=i,["name"]=k.name,["timer"]={0,0},["slot"]=0}
			member[i]=tmp
			member_nm[k.name]=tmp
			EveryBodyGuard.ActiveDisplay.UpdateMember(i,k)
			if(target==tmp.name)
			then
				EveryBodyGuard.BuffsChanged(tmp,GetBuffs(BuffFriendlyTarget),true)
				EveryBodyGuard.TargetChanged(target)
			else
				if(last_guard and last_guard.name==tmp.name)
				then
					EveryBodyGuard.ActivateGuard_Hard(tmp,{["effectIndex"]=last_guard.slot})
				end
			end
		else
			EveryBodyGuard.ActiveDisplay.UpdateMember(i)
		end
	end
	if(headid==0)then _Fader.FadeTo_MultiCallSafe(headwindow,0) end
	groupupdated=false
end

function EveryBodyGuard.GroupStatusUpdated(idx)
	local hit=member[idx]
	local data=PartyUtils.GetPartyData()[idx]
	if(hit and data)
	then
		EveryBodyGuard.ActiveDisplay.UpdateHealth(idx,data.healthPercent)
		EveryBodyGuard.HeadIcon(hit,data.worldObjNum)
	end
end

function EveryBodyGuard.HeadIcon(data,ID)
	if(not data or not EveryBodyGuard.Data.ShowHeadIcon)then return end
	if((guard==data and headid~=ID) or headidx==data.idx)
	then
		if(headid~=0)
		then
			DetachWindowFromWorldObject(headwindow,headid)
		else
			_Fader.FadeTo_MultiCallSafe(headwindow,1)
		end
		if(guard==data and ID~=0)
		then
			AttachWindowToWorldObject(headwindow,ID)
			headidx=data.idx
			headid=ID
		else
			_Fader.FadeTo_MultiCallSafe(headwindow,0)
			headidx=0
			headid=0
		end
	end
end

function EveryBodyGuard.FixOverHeadIcon()
	_Fader.FadeTo_MultiCallSafe(headwindow,guard and 1 or 0)
end

function EveryBodyGuard.TargetChangedEvent(targetClassification)
	if(targetClassification=="selffriendlytarget")
	then
		TargetInfo:UpdateFromClient()
		EveryBodyGuard.TargetChanged(TargetInfo:UnitName("selffriendlytarget"):match(L"([^^]+)^?([^^]*)"))			-- Thanks Aiiane, would have took me some time to get the regular expression right
	end
end

function EveryBodyGuard.TargetChanged(target)
	local hit=member_nm[target]
	if(target~=lasttarget and (hit or lasttarget))
	then
		local last=lasttarget
		lasttarget=target
		if(last)
		then
			local lasthit=member_nm[last]
			if(lasthit)
			then
				EveryBodyGuard.ActiveDisplay.UpdateTarget(lasthit.idx,false)
			end
		end
		if(hit)
		then
			EveryBodyGuard.ActiveDisplay.UpdateTarget(hit.idx,true)
		end
	end
end

function EveryBodyGuard.SelfBuffsChanged(BuffTable,isFullList)
	if(not BuffTable)then return end
	if(isFullList)
	then
		self_guard=nil
	end
	for i,k in pairs(BuffTable)
	do
		if(k.castByPlayer and k.abilityId==id)
		then
			self_guard=i
			break
		else
			if(not k.effectIndex and self_guard==i)
			then
				self_guard=nil
				for i,k in pairs(member)
				do
					EveryBodyGuard.DeactivateGuard_Hard(k)
				end
				break
			end
		end
	end
end

function EveryBodyGuard.FriendlyBuffsChanged(ttype,BuffTable,isFullList)
	if(lasttarget and ttype==BuffFriendlyTarget)
	then
		EveryBodyGuard.BuffsChanged(member_nm[lasttarget],BuffTable,isFullList)
	end
end

function EveryBodyGuard.GroupBuffsChanged(ttype,BuffTable,isFullList)
	EveryBodyGuard.BuffsChanged(member[ttype+1],BuffTable,isFullList,true)
end

function EveryBodyGuard.BuffsChanged(data,BuffTable,isFullList,checktrust)
	if(not BuffTable)then return end
	if(not data)
	then
		EveryBodyGuard.GroupUpdated()
		return
	end
	if(checktrust)
	then
		if(isFullList)
		then
			if(not trust)then return end
		else
			trust=true
		end
	end
	if(isFullList)
	then
		local hit=nil
		for i,k in pairs(BuffTable)
		do
			if(k.castByPlayer and k.abilityId==id)
			then
				hit=k
				break
			end
		end
		if(hit)
		then
			EveryBodyGuard.ActivateGuard_Hard(data,hit,checktrust)
		else
			if(data==guard)
			then
				EveryBodyGuard.DeactivateGuard_Hard(data,checktrust)
			end
		end
	else
		for i,k in pairs(BuffTable)
		do
			if(k.castByPlayer and k.abilityId==id)
			then
				EveryBodyGuard.ActivateGuard_Hard(data,k,checktrust)
				break
			else
				if(not k.effectIndex and data.slot==i)
				then
					EveryBodyGuard.DeactivateGuard_Hard(data,checktrust)
					break
				end
			end
		end
	end
end

function EveryBodyGuard.ActivateGuard_Soft(data,blocked)
	if(data.timer[2]==0)
	then
		if(blocked)
		then
			data.timer[1]=0.25+timer
			EveryBodyGuard.ActiveDisplay.GuardBlocked(data.idx)
		else
			data.timer[1]=softlag+timer
			EveryBodyGuard.ActiveDisplay.UpdateSoftGuard(data.idx,true)
		end
	end
end

function EveryBodyGuard.DeactivateGuard_Soft(data)
	EveryBodyGuard.ActiveDisplay.UpdateSoftGuard(data.idx,false)
end

function EveryBodyGuard.ActivateGuard_Hard(data,buff,checktrust)
	if(checktrust and guard and guard.name~=data.name)
	then
		trust=false
		return
	end
	for i,k in pairs(member)
	do
		if(i~=data.idx)
		then
			EveryBodyGuard.DeactivateGuard_Hard(k)
		end
	end
	if(data.timer[1]>0)
	then
		local old=softlag
		local lag=softlag-data.timer[1]+timer
		if(lag>(softlag+lowerlimit)/10)
		then
			softlag=softlag+lag/3
			if(softlag>upperlimit)
			then
				softlag=upperlimit
			end
		else
			if(lag<softlag/8)
			then
				softlag=softlag-((softlag-lag)/8)
				if(softlag<lowerlimit)
				then
					softlag=lowerlimit
				end
			end
		end
	else
		if(data.timer[2]==0)
		then
			softlag=softlag+upperlimit/5
			if(softlag>=upperlimit)
			then
				softlag=upperlimit*1.1
			end
		end
	end
	data.timer[1]=0
	data.timer[2]=hardlag+timer
	if(buff)then data.slot=buff.effectIndex end
	guard=data
	EveryBodyGuard.ActiveDisplay.UpdateHardGuard(data.idx,true)
	EveryBodyGuard.HeadIcon(data,PartyUtils.GetPartyData()[data.idx].worldObjNum)
end

function EveryBodyGuard.DeactivateGuard_Hard(data,checktrust)
	if(checktrust and guard)
	then
		trust=false
		return
	end
	if(guard==data)
	then
		data.timer[1]=0
		data.timer[2]=0
		data.slot=0
		guard=nil
		EveryBodyGuard.DeactivateGuard_Soft(data)
		EveryBodyGuard.ActiveDisplay.UpdateHardGuard(data.idx,false)
		EveryBodyGuard.HeadIcon(data,PartyUtils.GetPartyData()[data.idx].worldObjNum)
	end
end

do
	local none=GameData.PlayerActions.NONE
	local trigger_guard=GameData.PlayerActions.DO_ABILITY
	local set_target=GameData.PlayerActions.SET_TARGET
	local WindowSetGameActionData=WindowSetGameActionData

	function EveryBodyGuard.UpdateInteraction(data)
		action_set=false
		if(member_nm[lasttarget] and data.idx==member_nm[lasttarget].idx)
		then
			action_blocked=blocker>0
			if(action_blocked or data.timer[1]>0 or data.timer[2]>0)
			then
				WindowSetGameActionData(EveryBodyGuard.ActiveDisplay.GetActionWindow(data.idx),none,0,L"")
			else
				WindowSetGameActionData(EveryBodyGuard.ActiveDisplay.GetActionWindow(data.idx),trigger_guard,id,L"")
				action_set=true
			end
		else
			action_blocked=false
			WindowSetGameActionData(EveryBodyGuard.ActiveDisplay.GetActionWindow(data.idx),set_target,0,data.name)
		end
	end
end

function EveryBodyGuard.BeginCast(cid)
	if(cid==id)
	then
		local lasthit=member_nm[lasttarget]
		if(lasthit)
		then
			EveryBodyGuard.ActivateGuard_Soft(lasthit)
		end
	end
	blocker=1.5
end

function EveryBodyGuard.AbilityToggled(aid,active)
	if(aid==id)
	then
		if(active and not guard)
		then
			local hit
			for i,k in ipairs(member)
			do
				if(k.timer[1]>0)
				then
					if(not hit or hit.timer[1]<k.timer[1])
					then
						hit=k
					end
				end
				if(hit)then EveryBodyGuard.ActivateGuard_Hard(hit) end
			end
		else
			if(not active and guard)then EveryBodyGuard.DeactivateGuard_Hard(guard) end
		end
	end
end

function EveryBodyGuard.PreClick(idx)
	local hit=member[idx]
	if(hit)
	then
		EveryBodyGuard.UpdateInteraction(hit)
	end
end

function EveryBodyGuard.Click(idx)
	local hit=member[idx]
	if(hit)
	then
		if(action_blocked)
		then
			if(hit.timer[1]<=0)
			then
				EveryBodyGuard.ActivateGuard_Soft(hit,true)
			end
			return
		end
		if(action_set)
		then
			if(guard)
			then
				EveryBodyGuard.DeactivateGuard_Hard(guard)
			else
				if(hit.timer[1]<=0)
				then
					EveryBodyGuard.ActivateGuard_Soft(hit)
				end
			end
		end
	end
end

function EveryBodyGuard.Assist(idx)
	if(InterfaceCore.GetGameVersion()=="1.3.3")
	then
		local hit=member[tonumber(idx)]
		if(hit)
		then
			SystemData.UserInput.ChatText=L"/assist "..hit.name
			BroadcastEvent(SystemData.Events.SEND_CHAT_TEXT)
		end
	end
end
