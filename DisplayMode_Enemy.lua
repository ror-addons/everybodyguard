local _Enemy={}
EveryBodyGuard.DisplayModes.Enemy=_Enemy

local EBG_cache
local Enemy_cache
local EBG_Dirty,Enemy_Dirty
local indicators
local colors=	{["soft"]={180,152,0},["hard"]={255,50,50},["blocked"]={230,230,230}}

local Enemy=Enemy
local function donothing() end		-- Enemy already handles that or we don't care

_Enemy.UpdateBinding=donothing
_Enemy.UpdateHealth=donothing
_Enemy.UpdateTarget=donothing

local function DestroyIndicator(idx)
	local name=indicators[idx]
	if(name)
	then
		DestroyWindow(name)
		indicators[idx]=nil
	end
end

local function CreateIndicator(idx,window)
	local name=window.."_EBGindocator"
	local oldname=indicators[idx]
	if(not oldname or oldname~=name)
	then
		if(oldname)
		then
			DestroyIndicator(idx)
		end
		CreateWindowFromTemplate(name,"EBG_Enemy_indicator",window)
		WindowAddAnchor(name,"bottomleft",window,"bottomleft",-2,2)
		WindowSetShowing(name,false)
		indicators[idx]=name
	end
end

local function SetIndicator(idx,c,showing)
	local name=indicators[idx]
	if(not name)then return end
	if(showing)
	then
		WindowSetTintColor(name.."Guard",c[1],c[2],c[3])
		Fader.FadeTo_MultiCallSafe(name,1,0.3)
	else
		Fader.FadeTo_MultiCallSafe(name,0,0.3)
	end
end

local function FlashIndicator(idx)
	local name=indicators[idx]
	if(not name)then return end
	local c=colors.blocked
	WindowSetTintColor(name.."Guard",c[1],c[2],c[3])
	Fader.FadeTo(name,1,0.06)
	Fader.FadeTo_MultiCallSafe(name,0,0.19,0.1,1)
end

local function UnitFramesOnMouse(e, mouseButton, isKeyDown, flags, frame)
	if(mouseButton~=1 or not(flags==nil or flags==0))then return end
	local player = Enemy.groups.players[frame.playerName]
	local EBG_data=EBG_cache[player.name]
	if(not EBG_data)then return end
	e.cancel=true
	if(isKeyDown)
	then
		EveryBodyGuard.PreClick(EBG_data.idx)
	else
		EveryBodyGuard.Click(EBG_data.idx)
	end
end

local function UpdateAll()
	Enemy_cache={}
	local EBG_data,player,frame,name,window
	for i=1,5
	do
		name=EBG_cache[i]
		if(name)
		then
			EBG_data=EBG_cache[name]
			player=Enemy.groups.players[name]
			frame=Enemy.unitFrames.frames[player.groupIndex][player.index]
			window=frame.windowName
			CreateIndicator(EBG_data.idx,window)
			SetIndicator(i,EBG_data.color,EBG_data.showing)
			EveryBodyGuard.ReApplyHotkeys(window,i)
			Enemy_cache[name]=frame
		else
			DestroyIndicator(i)
		end
	end
	EBG_Dirty=false
	Enemy_Dirty=false
end

local function EnemyGroupUpdate()
	if(EBG_Dirty)
	then
		UpdateAll()
	else
		Enemy_Dirty=true
		EveryBodyGuard.GroupUpdated()
	end
end

local function RegisterEnemyEvent()
	Enemy.AddEventHandler("EveryBodyGuard","GroupsUpdated",EnemyGroupUpdate)
	Enemy.AddEventHandler("EveryBodyGuard","UnitFramesOnMouse",UnitFramesOnMouse)
end

function _Enemy.Initialize()
	if(not _Enemy.IsAviable())then return end
	Enemy_cache={}
	EBG_cache={}
	EBG_Dirty=false
	Enemy_Dirty=false
	indicators={}
	if(Enemy.unitFrames)
	then
		RegisterEnemyEvent()
	else
		Enemy.AddEventHandler("EveryBodyGuard","UnitFramesInitialized",RegisterEnemyEvent)
	end
end

function _Enemy.Shutdown()
	Enemy_cache=nil
	EBG_cache=nil
	for i=1,5
	do
		DestroyIndicator(i)
	end
	indicators=nil
	if(Enemy.unitFrames)
	then
		Enemy.RemoveEventHandler("EveryBodyGuard","GroupsUpdated")
		Enemy.RemoveEventHandler("EveryBodyGuard","UnitFramesOnMouse")
	else
		Enemy.RemoveEventHandler("EveryBodyGuard","UnitFramesInitialized")
	end
end

function _Enemy.IsAviable()
	return Enemy~=nil and Enemy.Settings.version>=55
end

function _Enemy.GetActionWindow(idx)
	local playername=EBG_cache[idx]
	local frame=playername and Enemy_cache[playername]
	return frame and frame.windowName
end

function _Enemy.UpdateMember(idx,data)
	if(idx==1)
	then
		EBG_cache={nil,nil,nil,nil,nil}
	end
	if(data)
	then
		name=Enemy.FixString(data.name)
		EBG_cache[idx]=name
		EBG_cache[name]={["idx"]=idx,["color"]=colors.soft,["showing"]=false}
	end
	if(idx==5)
	then
		if(Enemy_Dirty)
		then
			UpdateAll()
		else
			EBG_Dirty=true
		end
	end
end

function _Enemy.UpdateSoftGuard(idx,guard)
	_Enemy.UpdateGuard(idx,guard,colors.soft)
end

function _Enemy.UpdateHardGuard(idx,guard)
	_Enemy.UpdateGuard(idx,guard,colors.hard)
end

function _Enemy.GuardBlocked(idx)
	FlashIndicator(idx)
end

function _Enemy.UpdateGuard(idx,showing,color)
	local name=EBG_cache[idx]
	SetIndicator(idx,color,showing)
	local data=EBG_cache[name]
	data.showing=showing
	data.color=color
end
